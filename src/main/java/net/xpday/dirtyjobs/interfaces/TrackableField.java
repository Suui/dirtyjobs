/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TrackableField extends Remote {
	/**
	 * ask a trackable gently to track a vehicle.
	 * 
	 * If the request is accepted the method returns a color that the vehicle takes on
	 * and the vehicle will start sending info using the protocol defined in Protocol and 
	 * CodecUtils to the specified host and port. 
	 * 
	 * All messages will contain the identification byte passed here. 
	 * 
	 * @param host the host to send protocol messages to 
	 * @param port the port to send protocol messages to
	 * @param identification
	 * @return the rgb value of the color the sending vehicle takes on.
	 * @throws RemoteException
	 */
	public abstract Integer trackVehicle(String host, int port,
			byte identification) throws RemoteException;

}