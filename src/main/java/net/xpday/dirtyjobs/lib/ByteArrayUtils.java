/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

public class ByteArrayUtils {

	private static final char[] Nibble = new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

	/**
	 * Dumps the passed byteArray to an upppercase hexedecimal string like
	 * 
	 * 01AAFF2F
	 *   
	 * @param byteArray
	 * @return
	 */
	public static String hexDump(byte[] byteArray) {
		return hexDump(byteArray, 0, byteArray.length);
	}

	/**
	 * Dumps the specified part of the passed byteArray to an upppercase hexedecimal string like
	 * 
	 * 01AAFF2F
	 *   
	 * @param byteArray
	 * @param from
	 * @param count
	 * @return
	 */
	public static String hexDump(byte[] byteArray, int from, int count) {
		return hexDump(byteArray, from, count, (char) 0x00);
	}

	/**
	 * Dumps the specified part of the passed byteArray to an upppercase hexedecimal string like
	 * 
	 * 01|AA|FF|2F
	 *  
	 *  (in case the separation char was a pipe symbol) 
	 *   
	 * @param byteArray
	 * @param from
	 * @param count
	 * @param separationChar
	 * @return
	 */
	public static String hexDump(byte[] byteArray, int from, int count, char separationChar) {
		if (from >= byteArray.length)
			return ""; 
		count = Math.min(count, byteArray.length - from);
		StringBuffer buffer = new StringBuffer(3*count);
		int end = from + count;
		for (int i = from; i < end; i++) {
			byte b = byteArray[i];
			if (i>from && separationChar > 0) buffer.append(separationChar);
			buffer.append(Nibble[(b & 0xF0) >> 4]); 
			buffer.append(Nibble[(b & 0x0F)]);
		}
		return new String(buffer);
	}
	/**
	 * Dumps the passed byteArray to an upppercase hexedecimal string like
	 * 
	 * 01|AA|FF|2F
	 *  
	 *  (in case the separation char was a pipe symbol) 
	 *   
	 * @param byteArray
	 * @param separationChar
	 * @return
	 */
	public static String hexDump(byte[] byteArray, char separationChar) {
		return hexDump(byteArray, 0, byteArray.length, separationChar);
	}

}
