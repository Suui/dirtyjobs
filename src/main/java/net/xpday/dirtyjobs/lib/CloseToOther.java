/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

/**
 * enumeration class defining wether a vehicle is close to another.
 * (how close close is is unspecified)
 * 
 */
public class CloseToOther {

	public static final CloseToOther No = new CloseToOther(false);
	public static final CloseToOther Yes = new CloseToOther(true);
	private boolean closing;

	private CloseToOther(boolean b) {
		closing = b;
	}

	@Override
	public String toString() {
		return closing ? "Closing in on others" : "Not closing in on others";
	}

}
