/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

/** 
 * A speed utility class. While the static final constan may indicate a enum class, it is not.  
 */
public class Speed {

	private int value;

	public Speed(int value) {
		this.value = value;
	}

	public static final Speed Nul = new Speed(0);
	public static final Speed Fourty = new Speed(40);
	public static final Speed Twenty = new Speed(20);
	public static final Speed Eighty = new Speed(80);
	public static final Speed Hundred = new Speed(100);

	public static Speed[] possibilities = {Twenty, Fourty, Eighty};

	public int distanceOver(int iterationLength) {
		return value * iterationLength / 1000;
	}
	
	@Override
	public String toString() {
		return "Speed("+value+")";
	}
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Speed)) return false;
		Speed other = (Speed)obj;
		
		return value == other.value;
	}

	public int getValue() {
		return value;
	}

}
